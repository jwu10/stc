#!/bin/bash

objects=`mysqldata -s -N -e "select count(*) from local.device_object where device_id = (select id from deviceinfo where name like '%10.129.17.99%');"`;
indicators=`mysqldata -s -N -e "select count(*) from local.device_indicator where device_id = (select id from deviceinfo where name like '%10.129.17.99%');"`;
echo $objects
echo $indicators

if [ $objects = 5 -a $indicators = 65 ]; then
		echo -e "Pass"
	else
		echo "Fail"
		if [ $objects = 5 ]; then
				echo -e  "Correct number of objects created"
		else
			echo -e "Incorrect object count"
		fi
fi

