deploySTC deploys a brand new STC

configureNMS changes the output of the STC into NMS

sendDevice creates one device with 5 objects

The 3 files listed above are all functions in nmsScriptCreateAndSend.sh
They are separate files to help make intermediate tests simpler, configureNMS and sendDevice are run with the IP of the stc as an argument

verifyNMS verifies the NMS is getting one device with 5 objects from the NMS. 

deleteDevice deletes the device from the STC, device still needs to be manually removed from the NMS.

zaxxonScript and verifyZaxxon can be ignored
